package com.apiservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ApiServicesApplication {

	private static Logger logger = LoggerFactory.getLogger(ApiServicesApplication.class);

	public static void main(String[] args) {
		logger.info("[API Cadastro de Clientes] | Iniciando a api do cliente!");
		SpringApplication.run(ApiServicesApplication.class, args);
		logger.info("[API Cadastro de Clientes] | API do cliente iniciada e pronta para receber requisições!");
	}

}
