package com.apiservices.dataprovider;

import com.apiservices.core.entity.ClienteEntity;
import com.apiservices.core.gateway.ClienteGateway;
import com.apiservices.dataprovider.entity.ClienteTable;
import com.apiservices.dataprovider.mappers.ClienteTableTesteMapper;
import com.apiservices.dataprovider.repository.ClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClienteDataProvider implements ClienteGateway {

    private static final Logger logger = LoggerFactory.getLogger(ClienteDataProvider.class);

    private ClienteRepository clienteRepository;

    @Autowired
    public ClienteDataProvider(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @Override
    public ClienteEntity salvar(ClienteEntity cliente) {
        logger.info("[Cliente] | Criação do cliente - Inicio");
        ClienteTable clienteTable;
        try {
            clienteTable = clienteRepository.save(ClienteTableTesteMapper.toTable(cliente));
            logger.info("[Cliente] | Criação do cliente - Sucesso");
        } catch (Exception e) {
            logger.error("[Cliente] | Criação do cliente - Erro");
            throw new RuntimeException(e.getMessage());
        }
        return ClienteTableTesteMapper.toEntity(clienteTable);
    }

    @Override
    public void deleteById(Long id) {
        validaCliente(id);
        clienteRepository.deleteById(id);
    }

    @Override
    public List<ClienteEntity> obterClientes() {
        List<ClienteTable> table = clienteRepository.findAll();
        return table.stream()
                .map(entity -> ClienteTableTesteMapper.toEntity(entity))
                .collect(Collectors.toList());
    }

    @Override
    public ClienteEntity obterClientePorId(Long id) {
        validaCliente(id);
        return ClienteTableTesteMapper.toEntity(clienteRepository.getOne(id));
    }

    @Override
    public ClienteEntity ataulizarCliente(ClienteEntity cartaoEntity, Long id) {
        ClienteTable clienteTable;
        try {
            validaCliente(id);
            logger.info("[CLIENTE] | Atualizas cliente - Inicio");
            clienteTable = ClienteTableTesteMapper.toTable(cartaoEntity);
            clienteTable.setDataHoraAlteracao(LocalDateTime.now());
            clienteRepository.save(clienteTable);
        } catch (Exception e) {
            logger.error(String.format("[CLIENTE] | Erro no processamento do servidor"));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        logger.info("[CLIENTE] | Cliente Id: " + id + " alterado com sucesso!!");
        return ClienteTableTesteMapper.toEntity(clienteTable);
    }

    public void validaCliente(Long id){
        if (!clienteRepository.findById(id).isPresent()){
            logger.info("[Cliente] | Cliente não existe no banco de dados");
            throw new RuntimeException("Cliente não existe no banco de dados");
        }
    }
}
