#TAG=1.0.0
TAG=${shell git log -1 --format=%h}

build:
	docker build -t api-service ./
login:
	docker login
tag: login
	docker tag api-service souz4alan/api-service:$(TAG)
push: tag
	docker push souz4alan/api-service:$(TAG)