FROM openjdk:8

## Para debugar o projeto no container
#ARG PROFILE
#ARG ADDITIONAL_OPTS
#
## Para debugar o projeto no container
#ENV PROFILE=${PROFILE}
#ENV ADDITIONAL_OPTS=${ADDITIONAL_OPTS}

WORKDIR /api-service

COPY /target/api-services*.jar api-service.jar

#SHELL ["/bin/sh", "-c"]

# expondo porta tomcat
EXPOSE 8081
## expondo porta que conecta o debug
#EXPOSE 5005

CMD java -jar api-service.jar

#CMD java ${ADDITIONAL_OPTS} -jar api-service.jar --spring.profiles.active=${PROFILE}
